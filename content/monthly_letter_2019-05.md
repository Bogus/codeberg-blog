Title: Monthly Report April 2019
Date: 2019-05-15
Category: Monthly Letter
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V one week ago)


Dear Codeberg e.V. Members and Supporters!

Again it is time for some updates. April was a slow month, family life and Easter holidays demanded our attention: Some of our codeberg-goals are delayed, and have to be postponed. Let's start with the numbers. 

Codeberg e.V. has 32 active members. We are hosting 553 repositories, created and maintained by 498 users. Compared to one month ago, this is an organic growth of rate of +76 repositories (+16% month-over-month), and +37 users (+8%).

The machines are runnnig at about 20% capacity, we will scale up as soon we approach the 66% threshold. Backups are still managed by private offline machines by founding members at no cost; as soon Codeberg e.V. can afford it's own infrastructure, we are planning for an economic mid-end server with extendable redundant RAID disks, hardware donations may or may not allow us to afford those earlier. For the live server, our models suggest that rented cloud VMs are the most economic option for up to ~1000 users/repos, from then on it would potentially make sense to switch to rented dedicated servers.

What did we achieve this month? Codeberg.org has incorporated first bits of our new visual design. A great shoutout to all involved in design discussion, and in particular to @momar, who worked out all details and created the pull request for the launch and deployment files!

In this process, to facilitate testing and feedback, we allocated the testing domain codeberg-test.org, to have a publicly accessible testnet facility, in addition to testing in local VMs created on request on your local machines (which was great especially for offline development when traveling, but a bit tricky to set up). There is no red warning banner in place yet (configuration is 100% identical to the production net right now), so be aware that as in typical testnets, all data can vanish at any point in time.

On the negative side, we have not achieved our goal to work out the detailed bylaw change proposal. As we wrote in our previous update, we need to incorporate a minor bylaw change to get recognized as Non-Profit organization by tax authorities. We will propose this change as pull request, and will ask our members for vote via email. Working out the change proposal is our highest priority now.

Gitea's developers fixed the critical security issue reported by @ashimokawa (2FA bypass by API, #32), now API access via apps like GitNex (https://gitnex.com) is working through two-factor-authenticated channels.

Our hopes to test and enable use of Gitea's new oauth2 provider feature has been delayed, we hope to come back to this next month.

Moving forward with our plan to open all non-sensitive code and data that is running Codeberg.org, we switched the blog code and content repo public, please see https://codeberg.org/Codeberg/blog. We are looking forward to your contributions!

We are still waiting for feedback from the Gitea developer team for our pull request that introduces public editable wikis (enabled and controlled in the repository settings).

Long-term, it would be great to have informative English translations of all relevant documents (some official documents are still German-only as this is the language required by local authorities). If you would like to contribute, translations, PRs in the org repository are very welcome!

Your Codeberg e.V.

