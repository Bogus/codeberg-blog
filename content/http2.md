Title: Codeberg now supports http2!
Date: 2019-02-15
Category: Announcement
Authors: Andreas Shimokawa

After successful tests, we finally updated our servers to haproxy 1.8 and
enabled http2 support.

Enjoy faster page loads on codeberg.org :)
