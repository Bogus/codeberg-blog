Title: Monthly Report August 2019
Date: 2019-09-06
Category: Monthly Letter
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month)

Dear Codeberg e.V. Members and Supporters!

It is time for updates.

The poll for our bylaw change has approved the proposed pull request (https://codeberg.org/Codeberg/org/pulls/2) with 28 yes, 0 no, and 4 abstention votes. The change is now merged into the charter text, and an appointment at the notary office for registration of change is scheduled for first week of October. After formal confirmation by tax officers we will then be allowed to hand out tax deduction forms ("Bestätigung über Geldzuwendungen/Mitgliedsbeitrag") to members and sponsors, and are as legal entity tax-exempt for some of our operations.

Codeberg e.V. has 38 active members. We are hosting 1038 repositories, created and maintained by 1046 users. Compared to one month ago, this is an organic growth of rate of +131 repositories (+14% month-over-month), and +257 users (+32%).

The machines are runnnig at about 46% capacity, we will scale up as soon we approach the 66% threshold. Backups are still managed by private offline machines by founding members at no cost; as soon Codeberg e.V. can afford it's own infrastructure, we are planning for an economic mid-end server with extendable redundant RAID disks, hardware donations may or may not allow us to afford those earlier. For the live server, our models suggested that rented cloud VMs are the most economic option for up to ~1000 users/repos, from then on it would potentially make sense to switch to rented dedicated servers. As we have now passed this threshold, we can now re-run initial calculations and check options for a logical next step.

Codeberg e.V.

