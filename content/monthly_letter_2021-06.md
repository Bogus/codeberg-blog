Title: Monthly Report May 2021 - Preparing the new server
Date: 2021-06-16
Category: Monthly Letter
Authors: Codeberg e.V.

*(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V. this month; as usual published here with due delay. Not member of the non-profit association yet? [Consider joining it](https://join.codeberg.org)!)*


Dear Codeberg e.V. Members and Supporters, once again it is time for some updates!

We are hosting 12643 repositories, created and maintained by 11387 users. Compared to one month ago, this is an organic growth rate of +1051 repositories (+9.1% month-over-month) and +858 users (+8.1%).

**Server order:** The delivery of the server we ordered was delayed due to high demand of electronics and some parts being out of stock, it was shipped by the end of May. We are currently setting it up and preparing to move it to the datacenter at IN Berlin in the coming days. At first, it will mainly be used for providing additional services like CI, Codesearch and more. Codeberg members are kept up to date in an internal Discussion tracker.

**Attacks:** We find ourselves attacked several times each month. This often involves some IP addresses cloning the biggest Codeberg repos concurrently until they are blocked (we saw one attack with larger impact on June 7th). One time this month, we also  encountered a more creative approach: Some of you reported that the initial connection to Codeberg was slow, and we had to learn that we were not sufficiently protected to [TCP SYN floods](https://en.wikipedia.org/wiki/SYN_flood) yet. If you are running your own medium-sized server, make sure to check this! 

**Performance:** After we deployed Gitea 1.14 last months, we intitially celebrated a sudden drop in RAM usage because of a backend migration from go-git to plain Git. Over this month, we were investigating the overall site's performance dropping and several users reporting severe issues. Browsing the trees including the display of the last commit touching each file and folder is a big bottleneck for repos with long commit history, and this operation became much slower with native Git (the Gitea usage of this operation is not yet fully optimized). We migrated back to go-git for now as our machine is currently equipped with enough RAM to handle the high memory requirements. We are trying to further improve the backend so that Codeberg remains fast for everyone.  
You can follow the progress at [Codeberg/Community#453](https://codeberg.org/Codeberg/Community/issues/453), and maybe even join the efforts. Thank you!

**Malware campaigns:** At the beginning of May we have seen the Codeberg Pages feature involved in malware campaigns for India, usually providing "Covid vaccination apps" or "Pro versions" of common apps. The distributed Android apps were **not** hosted on Codeberg, but the pages used as landing pages and redirects to the downloads. We have been initially made aware of this by Indian offices and individual persons and since been looking at this closely. As we noticed that many users did not understand the issued warning pages, we have asked a security researcher for a Hindi translation of the takedown banner. Still, some users continued to register at Codeberg immediately after the takedowns and we assume they might still have been in search of their promised vaccination.
The malware campaigns stopped to abuse our service after we have decreased the detection time from ~12 hours to about an hour in the end. For us it's shocking to see how many users were hit by these campaigns (before the Hindi translation, we have received several emails per minute asking for help with the content) and how the precarious pandemic situation is abused by criminals to exploit people.
Stay safe and responsible in these times!

Codeberg e.V. has 129 members in total, these are 96 members with active voting rights and 32 supporting members, and one honorary member.

Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this account group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username. If you need access to Codeberg e.V. report documents without being listed in the account group, please send an email.

Your Codeberg e.V.

--<br/>

https://codeberg.org<br/>
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929. 
