Title: Monthly Report November 2019
Date: 2019-12-07
Category: Monthly Letter
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month)

Dear Codeberg e.V. Members and Supporters!

Again it is time for updates.

Our annual general assembly will take place on January 19th in Berlin. Save the date!

No worries if you cannot make it. Every active member will be able to exercise all rights remotely and online, even asynchronuously to avoid discrimination due to time zone discrepancies. Proposals for vote can be submitted via email or live, and all votes will be cast via voting tokens sent out to all active members in the week after the meeting. The advantage of physical attendance? Having a nice meet-up face to face, learn to know each other in person. Watch your inbox for the formal invite.

Slides and video of Andreas' Codeberg talk @sfscon are now online, check them out at https://www.sfscon.it/talks/codeberg-a-free-home-for-free-projects/.

Codeberg e.V. has 49 active members. We are hosting 1788 repositories, created and maintained by 4201 users. Compared to one month ago, this is an organic growth of rate of +206 repositories (+13% month-over-month), and +671 users (+19%).

The machines are runnnig at about 50% capacity, we will scale up as soon we approach the 66% threshold. Backups are still managed by private offline machines by founding members at no cost; as soon Codeberg e.V. can afford it's own infrastructure, we are planning for an economic mid-end server with extendable redundant RAID disks, hardware donations may or may not allow us to afford those earlier. For the live server, our models suggested that rented cloud VMs are the most economic option for up to ~1000 users/repos, from then on it would potentially make sense to switch to rented dedicated servers. As we have now passed this threshold, we can now re-run initial calculations and check options for a logical next step.

Your Codeberg e.V.

--
https://codeberg.org
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany
Registered at registration court Amtsgericht Charlottenburg VR36929. 

