Title: Codeberg.org launched!
Date: 2019-01-01
Category: Announcement
Authors: Codeberg Team


### TLDR; TeaHub is now Codeberg

Welcome to Codeberg.org! Please join us on our journey to build and support a safe and reliable home for Free and Open Source Software!

TeaHub is now Codeberg. Create your repository on Codeberg.org. Don't forget to update your remotes for existing repos:

```
git remote rm <oldTeaHubURI>
git remote add <newCodebergURI>
```

## Announcing Codeberg.org

We are proud to finally announce the launch of Codeberg.org, the Non-Profit Collaboration Community for Free and Open Source Projects (formerly known under its working title teahub.io). 

Codeberg is founded as a non-profit and non-government organization, with the objective to give the Open-Source code that is running our world a safe and friendly home, and to ensure that free code remains free and secure forever.

We invite you to join our society as an active or supporting member, in order to shape and guarantee the future of Open-Source development.

### The Mission

The development of Free and Open Source Software is experiencing an unbroken boom, due to the general availability of the internet and the resulting social network effects, multiplying communication, exchange of ideas, and productivity each and every month. The number of developers and projects participating in the Open-Source movement is growing exponentially. Only new software tools and collaboration platforms made these dynamics possible and manageable.

While all successful software tools that enabled this development were contributed by the Free and Open Source Software community, commercial for-profit platforms dominate the hosting of the results of our collaborative work. This has led to the paradox that literally millions of volunteers create, collect, and maintain invaluable knowledge, documentation, and software, to feed closed platforms driven by commercial interests, whose program is neither visible nor controllable from outside. Considering the fate of formerly successful startups like SourceForge, we need to break the circle and avoid history repeating.

The mission of the Codeberg&nbsp;e.V. is to build and maintain a free collaboration platform for creating, archiving, and preserving code and to document its development process.

Dependencies on commercial, external, or proprietary services for the operation of the platform are thus decidedly avoided, in order to guarantee independence and reliability.


### Legal Stuff

The single most significant risk for our undertaking was the final vote on the directive of the European Parliament and Council on copyright in the Digital Single Market. Happily, and only thanks to forceful protests, the directive has been amended at the last minute, and these important words were incorporated:

> "Providers of cloud services for individual use which do not provide direct access to the public, **open source software developing platforms,** and online market places whose main activity is online retail of physical goods, **should not be considered online content sharing service providers within the meaning of this Directive**"

To be clear: Without these amendments any Open-Source hosting platform would have been ruled out, and any project like ours would be impossible. A big Thank You to all involved!

### Money Stuff

Founding Codeberg&nbsp;e.V. and building the initial infrastructure has only been possible due to your donations to the founding team of the "Working Title TeaHub Project". We collected about 480€, not quite ICO levels, but more than sufficient to cover costs for legal documents, registration, infrastructure costs for the next weeks and months. Depending on usage and growth, we now depend on more structured ways of fundraising and membership support. Thus the Codeberg&nbsp;e.V. has been founded and registered according to German law.

Codeberg&nbsp;e.V. is organized as an idealistic non-profit organization, not as a speculative startup hoping for the next billion-dollar exit. This ensures our focus on stability and sustainability for the long run, but also implies that we, the community, are responsible for our own future.

We want to give our best and we count on your support to build this future in the best way we can imagine. Please join us and support Free and Open-Source Software development by joining the Codeberg&nbsp;e.V. as an active or supporting member, or by donating to our cause.

### What next

The first iteration of Codeberg.org in now running on rather limited resources. We will closely monitor our growth and make sure to extend infrastructure where sensible and necessary, but to do so responsibly and not waste funds entrusted to the Codeberg&nbsp;e.V.

We also have ideas for new services we could offer to free software projects – we will keep you updated in this blog. If you like to work with us in this area, please do not hesitate to contact us.

### Words of Warning

We are starting small and within constrained means. We will do our best to guarantee codeberg.org availability. You always should have backups of your data (which you probably have – you are a responsible person after all; and this is git). Please report all issues as they occur.

Parts of the underlying software stack, especially the user-facing frontend are at this point in time not as mature as commercially-funded counterparts. But given the tremendous track record of Open-Source software development, we are more than optimistic that these missing bits will catch up in no time.

### Finally

We thank you for your trust and your support. We wish you all a Happy New Year and we are looking forward to a great future.

**Join us at [Codeberg.org](https://codeberg.org)!**
