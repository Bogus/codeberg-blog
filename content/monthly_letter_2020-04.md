Title: Monthly Report March 2020
Date: 2020-04-20
Category: Monthly Letter
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month; as usual published here with due delay.)


Dear Codeberg e.V. Members and Supporters!

Time for updates.

On April 6th Codeberg.org was mentioned on HackerNews[1], which brought some attention. In the following days we hat a huge boost in daily registrations. Obviously there seems to be a lot of interest in a Free as in Freedom non-profit collaboration platform, but still a lot of people who never heard of codeberg. Let's spread the word.

Codeberg e.V. has 61 members in total, these are 46 members with active voting rights and 15 supporting members. We are hosting 2599 repositories, created and maintained by 2080 users. Compared to one month ago, this is an organic growth of rate of +131 repositories (+5.3% month-over-month) and +127 users (+6.5%).

Due to the Hackernews post we had another rush of users since then, as of today, 2496 users are maintaining 2887 repos.

The machines are runnnig at about 52% capacity, as usual we will scale up as soon we approach the 66% threshold. In the coming weeks we plan to purchase hardware for an economic mid-end backup server doing automated offsite backups (job currently running on machine provided by founding members). If you would like to help in this project -- configuring/building/setting up the box -- please tell us!

Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username.

Your Codeberg e.V.

[1] <https://news.ycombinator.com/item?id=22795930>

--

https://codeberg.org
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany
Registered at registration court Amtsgericht Charlottenburg VR36929. 

