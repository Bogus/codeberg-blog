Title: Monthly Report July 2019
Date: 2019-08-14
Category: Monthly Letter
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month)

Dear Codeberg e.V. Members and Supporters!

We are voting! Please check your inbox for your vote token! The voting system is running for the time being on https://polls.codeberg-test.org/, please don't be surprised that this is still on testing (after all, this is our first official poll!). Of course we are going to publish the source code of the voting system asap.

Again, it is overdue time for some updates. A lot has happened in the world. And we need to catch up with some overdue organizational issues.

We have migrated to Gitea 1.9, which comes with many improvements, including the blame-feature, and, most convenient, improved GitHub import. Also, we are now running the full stack on Debian 10 (buster). If you encountered any issues, please let us know.

Codeberg e.V. has 34 active members. We are hosting 907 repositories, created and maintained by 789 users. Compared to one month ago, this is an organic growth of rate of +178 repositories (+24% month-over-month), and +124 users (+18%).

The machines are runnnig at about 38% capacity, we will scale up as soon we approach the 66% threshold. Backups are still managed by private offline machines by founding members at no cost; as soon Codeberg e.V. can afford it's own infrastructure, we are planning for an economic mid-end server with extendable redundant RAID disks, hardware donations may or may not allow us to afford those earlier. For the live server, our models suggest that rented cloud VMs are the most economic option for up to ~1000 users/repos, from then on it would potentially make sense to switch to rented dedicated servers.

The vote for our bylaw change has started. Please check your inbox (and potentially spam folder), and cast your vote! Please don't hesitate to write in case of issues or questions.

Codeberg e.V.

